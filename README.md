# xtag

A program that, when given a list of Xbox gamertags, attempts to change your gamertag to one of them until it succeeds. 

## Usage
In order to use this program, run `xtag.py` with  Python3. As instructed, authenticate with your Microsoft account and then enter the desired gamertag. The program will constantly attempt to change your gamertag to the specified GT. Xbox's API imposes a rate limit, so the program will send 5 requests every 30 seconds.