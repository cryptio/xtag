"""
Copyright 2019 cpke

The following program implements an Xbox gamertag checker, written for dragon7x12
"""

import sys
import getpass
import time
from colorama import init, Fore

from xbox.webapi.api.client import XboxLiveClient
from xbox.webapi.api.provider.account import AccountProvider
from xbox.webapi.authentication.manager import AuthenticationManager
from xbox.webapi.authentication.two_factor import TwoFactorAuthentication, TwoFactorAuthMethods
from xbox.webapi.common.exceptions import AuthenticationException, TwoFactorAuthRequired


def read_gamertags(filename: str):
    """
    Read a list of Xbox gamertags from a file

    :param filename: The source file
    :return: list
    """

    with open(filename) as fd:
        lines = [line.rstrip('\n') for line in fd]
        return lines


def __input_prompt(prompt, entries=None):
    """
    Format an input prompt

    :param prompt: The prompt string
    :param entries: List of entries to choose from
    :return: str: user_input
    """

    prepend = ''

    if entries:
        assert isinstance(entries, list)
        prepend += 'Choose desired entry:\n'
        for num, entry in enumerate(entries):
            prepend += '  {}: {}\n'.format(num, entry)

    return input(prepend + prompt + ': ')


def two_factor_auth(auth_mgr, server_data):
    """
    Attempt to perform 2FA

    :param auth_mgr: The authentication mannager to use
    :param server_data: The server data received from the 2FA challenge
    :return:
    """

    otc = None
    proof = None

    two_fa = TwoFactorAuthentication(auth_mgr.session, auth_mgr.email_address, server_data)
    strategies = two_fa.auth_strategies

    entries = ['{!s}, Name: {}'.format(
        TwoFactorAuthMethods(strategy.get('type', 0)), strategy.get('display'))
        for strategy in strategies
    ]

    index = int(__input_prompt('Choose desired auth method', entries))

    if index < 0 or index >= len(strategies):
        raise AuthenticationException('Invalid auth strategy index chosen!')

    verification_prompt = two_fa.get_method_verification_prompt(index)
    if verification_prompt:
        proof = __input_prompt(verification_prompt)

    need_otc = two_fa.check_otc(index, proof)
    if need_otc:
        otc = __input_prompt('Enter One-Time-Code (OTC)')

    access_token, refresh_token = two_fa.authenticate(index, proof, otc)
    auth_mgr.access_token = access_token
    auth_mgr.refresh_token = refresh_token
    auth_mgr.authenticate()


def rate_limited(max_per_second):
    """
    A decorator for limiting a function call to a certain amount per second

    :param max_per_second: THe function call limit per second
    :return:
    """

    min_interval = 1.0 / float(max_per_second)

    def decorate(func):
        last_time_called = [0.0]

        def rate_limited_function(*args, **kargs):
            elapsed = time.perf_counter() - last_time_called[0]
            left_to_wait = min_interval - elapsed
            if left_to_wait > 0:
                time.sleep(left_to_wait)
            ret = func(*args, **kargs)
            last_time_called[0] = time.perf_counter()
            return ret

        return rate_limited_function

    return decorate


# Xbox rate limits you to 5 requests every 30 seconds, or 0.16/second
@rate_limited(0.16)
def claim_gamertag_rate_limited(account: AccountProvider, xuid, gamertag):
    """
    A wrapper for claim_gamertag with rate limiting

    :param account:
    :param xuid:
    :param gamertag:
    :return:
    """

    return account.claim_gamertag(xuid, gamertag)


@rate_limited(0.16)
def change_gamertag_rate_limited(account: AccountProvider, xuid, gamertag):
    """
    A wrapper for change_gamertag with rate limiting

    :param account:
    :param xuid:
    :param gamertag:
    :return:
    """

    return account.change_gamertag(xuid, gamertag)


def main():
    init()  # initialize colorama

    gamertags = None

    two_factor_auth_required = False
    server_data = None

    try:
        gamertag_list = __input_prompt('[?] Enter the gamertag list filename')
        gamertags = read_gamertags(gamertag_list)
    except FileNotFoundError:
        print('{}[-] The specified file does not exist!{}'.format(Fore.RED, Fore.RESET))
        sys.exit(-1)
    except KeyboardInterrupt:
        print('{}[*] Keyboard interrupt received, exiting gracefully...{}'.format(Fore.YELLOW, Fore.RESET))
        sys.exit(0)
    except Exception as e:
        print('{}[-] Unexpected error: %s{}'.format(Fore.RED, Fore.RESET) % str(e))

    auth_mgr = AuthenticationManager()

    try:
        auth_mgr.email_address = __input_prompt('[?] Microsoft account email')
        auth_mgr.password = getpass.getpass('[?] Microsoft account password: ')
    except KeyboardInterrupt:
        print('{}[*] Keyboard interrupt received, exiting gracefully...{}'.format(Fore.YELLOW, Fore.RESET))
        sys.exit(0)

    try:
        auth_mgr.authenticate(do_refresh=True)
    except TwoFactorAuthRequired as e:
        print('{}[*] 2FA is required, message: %s{}'.format(Fore.YELLOW, Fore.RESET) % e)
        two_factor_auth_required = True
        server_data = e.server_data
    except AuthenticationException as e:
        print('{}[-] Email/Password authentication failed: %s{}'.format(Fore.RED, Fore.RESET) % e)
        sys.exit(-1)
    except Exception as e:
        print('{}[-] Unexpected error: %s{}'.format(Fore.RED, Fore.RESET) % str(e))
        sys.exit(-1)

    if two_factor_auth_required:
        try:
            two_factor_auth(auth_mgr, server_data)
        except AuthenticationException as e:
            print('{}[-] 2FA Authentication failed: %s{}'.format(Fore.RED, Fore.RESET) % e)
            sys.exit(-1)

    xbl_client = XboxLiveClient(auth_mgr.userinfo.userhash,
                                auth_mgr.xsts_token.jwt,
                                auth_mgr.userinfo.xuid)

    while True:
        for gamertag in gamertags:
            if len(gamertag) > 15:
                print('{}[-] Gamertag "%s" exceedes limit of 15 chars, skipping...{}'.format(Fore.RED, Fore.RESET))

            print('{}[*] Trying to change gamertag to \'%s\' for xuid \'%i\'...{}'.format(Fore.YELLOW, Fore.RESET) % (
                gamertag, xbl_client.xuid))

            # resp = xbl_client.account.claim_gamertag(xbl_client.xuid, gamertag)
            resp = claim_gamertag_rate_limited(xbl_client.account, xbl_client.xuid, gamertag)
            if resp.status_code == 409:
                print('{}[-] Gamertag %s is unavailable{}'.format(Fore.RED, Fore.RESET) % gamertag)
                # We can't change our gamertag to this, move on to the next GT
                continue
            elif resp.status_code == 429:
                sleep_time = int(resp.headers.get('Retry-After'))
                print('{}[*] Being rate limited, sleeping for %d seconds...{}'.format(Fore.YELLOW,
                                                                                      Fore.RESET) % sleep_time)
                time.sleep(sleep_time)

                continue
            elif resp.status_code != 200:
                print('{}[-] Invalid HTTP response from claim: %i{}'.format(Fore.RED, Fore.RESET) % resp.status_code)
                print('Headers: %s' % resp.headers)
                print('Response: %s' % resp.content)
                sys.exit(-1)

            print('{}[+] %s is available!{}'.format(Fore.GREEN, Fore.RESET) % gamertag)
            print('{}[+] Changing gamertag...{}'.format(Fore.GREEN, Fore.RESET))
            resp = change_gamertag_rate_limited(xbl_client.account, xbl_client.xuid, gamertag)
            if resp.status_code == 1020:
                print('{}[-] Changing gamertag failed - You are out of free changes{}'.format(Fore.RED, Fore.RESET))
                sys.exit(-1)
            elif resp.status_code == 429:
                sleep_time = int(resp.headers.get('Retry-After'))
                print('{}[*] Being rate limited, sleeping for %d seconds...{}'.format(Fore.YELLOW,
                                                                                      Fore.RESET) % sleep_time)
                time.sleep(sleep_time)
                                                                                                      
                continue
            elif resp.status_code == 403:
                print('{}[-] 403 Forbidden received, this gamertag is probably actually taken!{}'.format(Fore.RED,
                                                                                                         Fore.RESET))
                continue

            elif resp.status_code != 200:
                print('{}[-] Invalid HTTP response from change: %i{}'.format(Fore.RED, Fore.RESET) % resp.status_code)
                print('Headers: %s' % resp.headers)
                print('Response: %s' % resp.content)
                sys.exit(-1)

            print('{}[+] Gamertag successfully changed to %s{}'.format(Fore.GREEN, Fore.RESET) % gamertag)
            break


if __name__ == '__main__':
    main()
